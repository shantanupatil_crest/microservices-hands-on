import json
import sqlite3
import os
from flask import Flask, g, request

current_dir = os.path.dirname(os.path.abspath(__file__))

app = Flask(__name__)


def throw_server_error(e):
    return "Server Error Occurred: {}".format(e)


@app.route("/")
def main():
    return "Check README.md for usage instructions."


def db():
    try:
        db = getattr(g, "_database", None)
        if db is None:
            db = g._database = sqlite3.connect("database.db")
            cursor = db.cursor()
            query = (
                "SELECT name FROM sqlite_master WHERE"
                + " type='table' AND name='students'"
            )
            exists = query
            table_exists = cursor.execute(exists)
            if len(table_exists.fetchall()) == 0:
                query = (
                    "CREATE TABLE students "
                    + "(name text, roll real, mobile real, email text)"
                )
                cursor.execute(query)
        return db
    except Exception as e:
        return throw_server_error(e)


@app.route("/check_student", methods=["GET"])
def check_user():
    try:
        roll = float(request.args.get("roll"))
        query = "Select roll from students"

        cur = db().cursor()
        cur.execute(query)
        db_list = cur.fetchall()
        exists = False
        for item in db_list:
            if roll in item:
                exists = True
        db().commit()
        db().close()

        return str(exists)
    except Exception as e:
        return throw_server_error(e)


@app.route("/get_student", methods=["GET"])
def get_student():
    """This method return the student data
    based on roll provided given student exists

    Returns:
        student data: Complete data of student based on given roll
    """
    try:
        cur = db().cursor()
        student_roll = request.args.get("roll")

        query = f"SELECT * FROM students WHERE roll = {student_roll}"

        data = cur.execute(query).fetchall()[0]

        return f"{data}"
    except Exception as e:
        return throw_server_error(e)


@app.route("/display", methods=["GET"])
def display():
    try:
        query = "SELECT * FROM students"

        cur = db().cursor()
        student_data = cur.execute(query).fetchall()
        students = []
        for i in range(len(student_data)):
            temp_dict = {
                "name": student_data[i][0],
                "roll": student_data[i][1],
                "mobile": student_data[i][2],
                "email": student_data[i][3],
            }
            students.append(temp_dict)

        return json.dumps(students)
    except Exception as e:
        return throw_server_error(e)


@app.route("/new_student", methods=["POST"])
def add_student():
    try:
        name = request.form.get("name")
        roll = request.form.get("roll")
        mobile = request.form.get("mobile")
        email = request.form.get("email")

        cur = db().cursor()
        cur.execute(
            "INSERT INTO students VALUES ('{}','{}','{}','{}')".format(
                name, roll, mobile, email
            )
        )

        db().commit()
        db().close()

        return "Student Added Successfully"
    except Exception as e:
        return throw_server_error(e)


@app.route("/update_student", methods=["PUT"])
def update_student():
    """PUT Request: Update particular student data from student database

    Args:
        std_id (_type_): Particular student roll

    Returns:
        _type_: string
    """
    try:
        student_roll = request.form.get("roll")
        student_name = request.form.get("name")
        student_mobile_number = request.form.get("mobile")
        student_email = request.form.get("email")

        cur = db().cursor()

        if student_email is not None:
            query = "UPDATE students SET email = ? WHERE roll = ?"
            cur.execute(
                query,
                (
                    student_email,
                    student_roll,
                ),
            )
        if student_name is not None:
            query = "UPDATE students SET name = ?WHERE roll = ?"
            cur.execute(
                query,
                (
                    student_name,
                    student_roll,
                ),
            )
        if student_mobile_number is not None:
            query = "UPDATE students SET mobile = ? WHERE roll =?"
            cur.execute(
                query,
                (
                    student_mobile_number,
                    student_roll,
                ),
            )

        db().commit()
        db().close()

        return "Student Updated Successfully"
    except Exception as e:
        return throw_server_error(e)


@app.route("/delete_student", methods=["DELETE"])
def delete_user():
    try:
        roll = request.args.get("roll")
        query = f"DELETE FROM students WHERE roll = {roll}"
        cur = db().cursor()
        cur.execute(query)

        db().commit()
        db().close()

        return "Delete Successfull"
    except Exception as e:
        return throw_server_error(e)


if __name__ == "__main__":
    app.run(port=5001, debug=True)
