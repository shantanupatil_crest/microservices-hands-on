# Python Training: Day-22: Microservices

## Team-11

Akash Singh - akash@crestdatasys.com

Satyendra Singh - satyendra.singh@crestdatasys.com

Shantanu Patil - shantanu.patil@crestdatasys.com

Yuvraj Darekar - yuvraj.darekar@crestdatasys.com


## Problem Statement

Create 2 Microservices:

- This would expose an API layer to end user to achieve some tasks.

- This would provide APIs to the first service and will communicate with the database


Groups of 4 with 2 teams each of 2:


Requirements:

- Documentation of 4-8 apis of the services.

- Use flask/fast api for your APIs.

- Using proper modules and classes in individual services.

- When both the services are run, the services should be able to communicate with each other.



## API Information
> Microservice_01 is the exposed layer for the user.

> Microservice_02 is the hidden layer and should be accessed by Microservice_01 only.

### Microservice_01:

#### http://localhost:5000/get_student [GET]
This API will be used to fetch information of a new student.

This accepts the following keys in the request Params.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |


#### http://localhost:5000/display_all [GET]
This API will be used to find all the Students present in the DataBase.


#### http://localhost:5000/add_student [POST]
This API will be used to create a new student in the DataBase.

This accepts the following keys in the request Body.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |
| name | Full name of the Candidate. | True |
| mobile | 10 digit Contact number of the Candidate. | True |
| email | Personal Email address of the Candidate. | True |


#### http://localhost:5000/update_student [PUT]
This API will be used to find and Update a specific user from the DataBase.

This accepts the following keys in the request Params.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |
| name | Full name of the Candidate. | False |
| mobile | 10 digit Contact number of the Candidate. | False |
| email | Personal Email address of the Candidate. | False |


#### http://localhost:5000/delete_student [DELETE]
This API will be used to find and Delete a specific user from the DataBase.

This accepts the following keys in the request Params.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |


### Microservice_02:

#### http://localhost:5001/check_student [GET]
This API will be used to find if the Students present in the DataBase.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |


#### http://localhost:5001/get_student [GET]
This API will be used to fetch information of a new student.

This accepts the following keys in the request Params.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |


#### http://localhost:5001/display [GET]
This API will be used to find all the Students present in the DataBase.


#### http://localhost:5001/new_student [POST]
This API will be used to create a new student in the DataBase.

This accepts the following keys in the request Body.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |
| name | Full name of the Candidate. | True |
| mobile | 10 digit Contact number of the Candidate. | True |
| email | Personal Email address of the Candidate. | True |


#### http://localhost:5001/update_student [PUT]
This API will be used to find and Update a specific user from the DataBase.

This accepts the following keys in the request Params.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |
| name | Full name of the Candidate. | False |
| mobile | 10 digit Contact number of the Candidate. | False |
| email | Personal Email address of the Candidate. | False |


#### http://localhost:5001/delete_student [DELETE]
This API will be used to find and Delete a specific user from the DataBase.

| Key | Value  | Required  |
| :-----: | :-: | :-: |
| roll | Roll number of the Candidate in Integer format. | True |
| name | Full name of the Candidate. | False |
| mobile | 10 digit Contact number of the Candidate. | False |
| email | Personal Email address of the Candidate. | False |