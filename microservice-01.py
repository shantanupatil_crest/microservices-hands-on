from flask import Flask, request
import requests
import json

app = Flask(__name__)

MS2_URL = "http://127.0.0.1:5001/"
message_student_not_exists = "Given student does not exist!!!"


def check_user_existence(roll_number):
    res = requests.get(
        "{}check_student?roll={}".format(
            MS2_URL,
            roll_number,
        ),
    )
    return res.text


def throw_server_error(e):
    return "Server Error Occurred: {}".format(e)


@app.route("/")
def main():
    return "Check README.md for usage instructions."


@app.route("/get_student", methods=["GET"])
def get_student():
    try:
        student_roll = request.args.get("roll")

        if check_user_existence(student_roll) == "True":
            response2 = requests.get(
                "{}get_student?roll={}".format(
                    MS2_URL,
                    student_roll,
                )
            )
            return response2.text
        else:
            return message_student_not_exists

    except Exception as e:
        return throw_server_error(e)


@app.route("/display_all", methods=["GET"])
def display_all():
    try:
        response = requests.get(
            "{}display".format(MS2_URL),
        )
        response_list = response.json()
        json_data = json.dumps(response_list)
        return json_data
    except Exception as e:
        return throw_server_error(e)


@app.route("/add_student", methods=["POST"])
def add_student():
    try:
        student_roll = request.form.get("roll")
        student_name = request.form.get("name")
        student_mobile = request.form.get("mobile")
        student_email = request.form.get("email")
        if student_roll and student_name and student_mobile and student_email:
            body = {
                "roll": student_roll,
                "name": student_name,
                "mobile": student_mobile,
                "email": student_email,
            }

            if check_user_existence(student_roll) == "False":
                response2 = requests.post(
                    "{}new_student".format(MS2_URL),
                    data=body,
                )
                return response2.text
            else:
                return "Student Already Exists!"
        else:
            return "Incomplete Data"

    except Exception as e:
        return throw_server_error(e)


@app.route("/update_student", methods=["PUT"])
def update_user():
    try:
        student_roll = request.form.get("roll")
        student_name = request.form.get("name")
        student_mobile = request.form.get("mobile")
        student_email = request.form.get("email")
        body = {
            "roll": student_roll,
            "name": student_name,
            "mobile": student_mobile,
            "email": student_email,
        }

        if check_user_existence(student_roll) == "True":
            if (student_name or student_mobile or student_email) is not None:
                response2 = requests.put(
                    "{}update_student".format(MS2_URL),
                    body,
                )
                return response2.text
            else:
                return "Enter something to update !!!"
        else:
            return message_student_not_exists
    except Exception as e:
        return throw_server_error(e)


@app.route("/delete_student", methods=["DELETE"])
def delete_user():
    try:
        student_roll = request.form.get("roll")

        if check_user_existence(student_roll) == "True":
            response2 = requests.delete(
                "{}delete_student?roll={}".format(
                    MS2_URL,
                    student_roll,
                )
            )
            return response2.text
        else:
            return message_student_not_exists
    except Exception as e:
        return throw_server_error(e)


if __name__ == "__main__":
    app.run(port=5000, debug=True)
